package backup;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import sniffer.GUI;

public class Backup extends JFrame{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*
	 * basic components
	 */
JLabel jl1, jl2;
JButton  openDoor, insertCard, fingerPrint, insertACard;
JTextArea display;

public Backup(){
	Container container = getContentPane();
	/*
	 * Enclave
	 */
	JPanel title1 = new JPanel();
	JPanel enclaveUp = new JPanel();
	JPanel enclaveDown = new JPanel();
	JPanel enclave = new JPanel();
	
	jl2 = new JLabel("Enclave");//title
	title1.add(jl2);
	
	jl1 = new JLabel("TIS Admin Console Token Reader");//enclave up part
	insertACard = new JButton("Insert Card");
    enclaveUp.add(jl1);
	enclaveUp.add(insertACard);
	
	JLabel alarm = new JLabel("Alarm");                 //enclave down part
	JTextField alarmText= new JTextField("Alarm OFF");
	enclaveDown.add(alarm);
	enclaveDown.add(alarmText);
	
	enclave.add(title1);
	enclave.add(enclaveUp);
	enclave.add(enclaveDown);
	enclave.setBorder(new TitledBorder("Enclave"));// add border
	enclave.setLayout(new GridLayout(4,4));
	
	container.add(enclave, BorderLayout.NORTH);
	/*
	 * Door
	 */

	JPanel door = new JPanel();
	openDoor = new JButton("Open Door");
	Dimension preferredSize1 = new Dimension(100,30);
	openDoor.setPreferredSize(preferredSize1);
	JTextField doorText= new JTextField("Door is closed");
	door.add(doorText);
	door.add(openDoor);
	
	door.setBorder(new TitledBorder("Door"));// set door border
	
	container.add(door,BorderLayout.CENTER);
	
	/*
	 * Access Panel
	 */
	JPanel accessPanel = new JPanel();
	JPanel up = new JPanel();
	JPanel middle = new JPanel();
	JPanel down = new JPanel();
	
	display = new JTextArea();                     //display  up
	Dimension preferredSize2 = new Dimension(400,40);
	display.setPreferredSize(preferredSize2);
	up.add(display);
	
	Dimension preferredSize3 = new Dimension(180,40);// set size
	Dimension preferredSize4 = new Dimension(150,40);
	
	jl1 = new JLabel("Access Panel Token Reader");// middle
	jl1.setPreferredSize(preferredSize3);
	insertCard = new JButton("Insert Card");
    insertCard.setPreferredSize(preferredSize4);
	middle.add(jl1);
	middle.add(insertCard);
	
	jl2 = new JLabel("Biometric Input Device");//down
	jl2.setPreferredSize(preferredSize3);
	fingerPrint = new JButton("Present Fingerprint");
	fingerPrint.setPreferredSize(preferredSize4);
	down.add(jl2);
	down.add(fingerPrint);
	 
	accessPanel.add(up);
	accessPanel.add(middle);
	accessPanel.add(down);
	accessPanel.setBorder(new TitledBorder("Access Panel"));// add border
	accessPanel.setLayout(new GridLayout(4,4));
	
	container.add(accessPanel, BorderLayout.SOUTH);
	

}
public static void main(String[] args) {
	  GUI s = new GUI();  //initial the GUI
	  s.setSize(500,700);// initialize the size of GUI
	  s.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  s.setVisible(true);
	}
}
