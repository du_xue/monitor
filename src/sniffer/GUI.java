package sniffer;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class GUI extends JFrame{
	int selected;
	ArrayList<PacketInfo> infoList; 
	private Monitor monitor;
    private JPanel mainPanel;
	private JPanel upPanel;
	//private JPanel downPanel;
	private JComboBox comboBox;
	private JLabel label1;
	private JButton button;
	//private JTextField textField;
	private JTextArea textArea;
	private JScrollPane sbrText;
	private static final long serialVersionUID = 1L;
	/**
	 * This is Graphic User interface for monitoring
	 */
	public GUI()
	{
		super();
		selected = 0;
		monitor = new Monitor();
		infoList = new ArrayList<PacketInfo>();
		setSize(700, 500);
		create();
		//this.pack();
		this.setVisible(true);
	}
	
	public void create()
	{
	    label1 = new JLabel();
		mainPanel = new JPanel();
		mainPanel = (JPanel)this.getContentPane();
		upPanel = new JPanel();
		//downPanel = new JPanel();
		button = new JButton();
	    comboBox = new JComboBox();
	    textArea = new JTextArea(20,50);
	    textArea.setLineWrap(true);
	    sbrText = new JScrollPane(textArea);
	    sbrText.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		//textField = new JTextField();
		String[] adapters = new String[10];
		
		ArrayList<String> selection = monitor.getDeviceList(); 
		int i =0;
		for(String string: selection)
		{
			adapters[i] = string;
			i++;
		}
		System.out.println(adapters+"22");
		comboBox = new JComboBox(adapters);

		comboBox.setForeground(new Color(0, 100, 100));
		
		label1.setHorizontalAlignment(SwingConstants.LEFT);
		//label1.setForeground(new Color(0, 0, 255));
		label1.setText("Select Adapter You Want:");
		button.setText("Monitor Packets via Adapters");
		upPanel.add(label1);
		upPanel.add(comboBox);  
		
		upPanel.add(button);
		upPanel.add(sbrText);	
		 comboBox.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent event) {
	                //
	                // Get the source of the component, which is our combo
	                // box.
	                //
	                JComboBox comboBox = (JComboBox) event.getSource();

	                //
	                // Print the selected items and the action command.
	                //

	                selected = comboBox.getSelectedIndex();
	                Object selectedItem = comboBox.getSelectedItem();
	         
	                	textArea.setText("You choose ==>" + selectedItem);
	              

	            }
	        });
        button.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		       
		        textArea.setText("Yes!");
		        infoList = monitor.getPacket(selected);
		        String layout = "Start!!!! \n";
		        int i = 1;
		        for (PacketInfo p : infoList)
		        {
		        	
		        	System.out.println("this is p " + p);
		        	layout = layout + "*Packet " + i +" * ----> Time: " + p.getTime() +"\n" + "   " + "  " 
		        + "IP Source: "+ p.getIpSource() + "     IP Destination: " + p.getIpDes() + "\n" +"   " + "  "
		        +"TCP Source: " + p.getTcpSource() + "    TCP Destination: " + p.getTcpDes() +"\n" +"   " + "  "
		        +"Original: "+p.getOriginalLen()+"     ActualLength: " + p.getActualLen() + "\n" ;
		        	i++;
		        }
		        textArea.setText(layout);
		        layout = null;
		    }
		    
		});
		mainPanel.add(upPanel);

		
	
		//mainPanel.add(downPanel,BorderLayout.CENTER);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        GUI s = new GUI();
        s.setVisible(true);
	}

}
