package sniffer;

import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.DefaultTableModel;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.packet.format.FormatUtils;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;

public class Monitor {
	ArrayList<PcapIf> deviceList;
	StringBuilder errorMessage;
	ArrayList<PacketInfo> infoList;
	
	public Monitor()
	{
		deviceList = new ArrayList<PcapIf>();
		errorMessage = new StringBuilder();
		infoList = new ArrayList<PacketInfo>();
	}
	
	
	public ArrayList<String> getDeviceList()
	{
	    ArrayList<String> devices = new ArrayList<String>();
	    int check = Pcap.findAllDevs(deviceList, errorMessage);  
        if (check == Pcap.NOT_OK || deviceList.isEmpty()) { 
        	devices.add("No device found!");
        }  
        System.out.println(deviceList+"@@@before loop");
 
             for (PcapIf device : deviceList) {  
            	 System.out.println(device.getDescription());
                 devices.add( device.getDescription().toString() + device.getName()); 
        }
		return devices;
	}
	
	public ArrayList<PacketInfo> getPacket(int flag)
	{
		
		Pcap.findAllDevs(deviceList, errorMessage);  
		PcapIf device = deviceList.get(flag); // get the selected device
		/**************************
		 * open the selected device
		 **************************/
		int catchAll = 64 * 1024;           // Capture all packets
	    int openMode = Pcap.MODE_PROMISCUOUS; // set the mode
	    int timeout = 10 * 1000;           // 10 seconds in milliseconds  
	    Pcap pcap =  Pcap.openLive(device.getName(), catchAll, openMode, timeout, errorMessage);  
		
	   /********************************************************************
	    *  create a packet handler receiving packets from the libpcap loop.
	    *********************************************************************/ 
	    PcapPacketHandler<String> jpacketHandler = new PcapPacketHandler<String>(){
	    	 Tcp tcp = new Tcp();
	         Ip4 ip = new Ip4();
	         /*String ip_address = null;
	         String destination;
	         String source;*/
	         
	     
	        
	    	  public void nextPacket(PcapPacket packet, String user) {
	    		   int i =0;
	    			PacketInfo info = new PacketInfo();
				
					Date time = new Date(packet.getCaptureHeader()
							.timestampInMillis());
					// column 0 and 1
					info.setId("" + i);
					info.setTime(time.toString());

					packet.getHeader(ip);
					if (packet.hasHeader(ip))
					{
						// column 2 and 3
						info.setIpSource(FormatUtils.ip(ip.source()));
						info.setIpDes(FormatUtils.ip(ip.destination()));
					}

					packet.getHeader(tcp);
					if (packet.hasHeader(tcp)) 
					{
						// column 4 and 5
						info.setTcpSource(new Integer(tcp.destination()).toString());
						info.setTcpDes(new Integer(tcp.source()).toString());
					}
					// column 6 and 7
					info.setActualLen("" + packet.getCaptureHeader().caplen());  // Length actually captured  
					info.setOriginalLen("" + packet.getCaptureHeader().wirelen()); // Original length 
	    		 i++;
	    		 System.out.println(info);
	    		 infoList.add(info);
	    		   
	    	  };
	    };
	    
	    pcap.loop(4, jpacketHandler, "VPN Monitor!");  
	    // close the capture
        pcap.close();
        System.out.println(infoList);
	    
		return infoList;
	}

}
