package sniffer;

public class PacketInfo {
    String id;
    String time;
    String ipSource;
    String ipDes;
    String tcpSource;
    String tcpDes;
    String originalLen;
    String actualLen;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getIpSource() {
		return ipSource;
	}
	public void setIpSource(String ipSource) {
		this.ipSource = ipSource;
	}
	public String getIpDes() {
		return ipDes;
	}
	public void setIpDes(String ipDes) {
		this.ipDes = ipDes;
	}
	public String getTcpSource() {
		return tcpSource;
	}
	public void setTcpSource(String tcpSource) {
		this.tcpSource = tcpSource;
	}
	public String getTcpDes() {
		return tcpDes;
	}
	public void setTcpDes(String tcpDes) {
		this.tcpDes = tcpDes;
	}
	public String getOriginalLen() {
		return originalLen;
	}
	public void setOriginalLen(String originalLen) {
		this.originalLen = originalLen;
	}
	public String getActualLen() {
		return actualLen;
	}
	public void setActualLen(String actualLen) {
		this.actualLen = actualLen;
	}
	@Override
	public String toString() {
		return "PacketInfo [id=" + id + ", time=" + time + ", ipSource="
				+ ipSource + ", ipDes=" + ipDes + ", tcpSource=" + tcpSource
				+ ", tcpDes=" + tcpDes + ", originalLen=" + originalLen
				+ ", actualLen=" + actualLen + "]";
	}

}
